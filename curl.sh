export AUTH_RESPONSE=$(curl --request POST \
  --data '{"jwt": "'"$VAULT_ID_TOKEN"'", "role":"myproject-staging"}' \
  $VAULT_SERVER_URL/v1/auth/jwt/login)

export VAULT_TOKEN=$(echo $AUTH_RESPONSE | jq -r .auth.client_token)

echo $AUTH_RESPONSE
echo $VAULT_TOKEN
export SECRET_PATH=secret/data/myproject/staging/db

SECRET=$(curl --silent --header "X-Vault-Token: $VAULT_TOKEN" \
  --request GET \
    $VAULT_SERVER_URL/v1/$SECRET_PATH | jq .data.data)
  
echo $SECRET